---
firstname: ""
lastname: ""
title: ""
---

## Activities
```dataview
TABLE WITHOUT ID
  ("[[" + file.path + "|" + title + "]]") AS "Activity",
  location AS "Location",
  type AS "Type",
  (date + " - " + date-end) AS "Date",
  people AS "People"
WHERE contains(join(people),this.firstname) AND category = "activity"
```
