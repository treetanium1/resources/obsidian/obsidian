---
title: "{{VALUE}}"
author: "{{VALUE:author}}"
publisher: "{{VALUE:publisher}}"
journal: "{{VALUE:journal}}"
year: "{{VALUE:year}}"
doi: "{{VALUE:doi}}"
visibility: public
---

# `= this.title`

by `=this.author` | `= ("[DOI](https://doi.org/" + this.doi + ")")`
