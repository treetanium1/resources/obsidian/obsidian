---
title: "{{VALUE}}"
location:
  - "[[{{VALUE:location}}]]"
people:
  - ""
---

## Activities

```dataview
TABLE WITHOUT ID
  ("[[" + file.path + "|" + title + "]]") AS "Activity",
  location AS "Location",
  meeting AS "Event",
  date AS "Date",
  join(people) AS "People"
WHERE contains(join(organisations),this.file.name) AND meeting
```
