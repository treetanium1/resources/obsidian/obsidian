---
type: staff
title: "{{VALUE:firstname}} {{VALUE:lastname}}"
firstname: "{{VALUE:firstname}}"
lastname: "{{VALUE:lastname}}"
department: "{{VALUE:department}}"
unit: "{{VALUE:unit}}"
keywords: "{{VALUE:keywords}}"
aliases:
  - "{{VALUE:firstname}} {{VALUE:lastname}}"
---

## Meetings

```dataview
TABLE WITHOUT ID
file.link AS "Meeting",
summary AS "Summary"
WHERE contains(join(meeting), this.file.name) OR contains(join(people), this.file.name)
```
