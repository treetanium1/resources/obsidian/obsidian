---
title: "{{VALUE}}"
date: "{{DATE:YYYY-MM-DD}}"
type: software
category:
  - "{{VALUE:category}}"
url: "{{VALUE:url}}"
docs: "{{VALUE:docs}}"
source: "{{VALUE:source}}"
desc-short: |
  {{VALUE:desc}}
tags:
---
# {{name}}
