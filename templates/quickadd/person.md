---
title: "{{VALUE:firstname}} {{VALUE:lastname}}"
firstname: "{{VALUE:firstname}}"
lastname: "{{VALUE:lastname}}"
alias: "{{VALUE:firstname}} {{VALUE:lastname}}"
---

## Events

```dataview
TABLE WITHOUT ID
  link(file.name, title) AS "Activity",
  join(location) AS "Location",
  date AS "Date",
  join(people) AS "People"
  WHERE contains(join(people),this.file.name) AND (category = "activity" or activity)
```
