---
title: "Property {{VALUE:LOCATION}}: {{NAME}}"
url: "{{VALUE:URL}}"
address: "{{VALUE:ADDRESS}}"
status: "{{VALUE:STATUS}}"
img: ""
size: {{VALUE:SIZE}}
public-transport: "{{VALUE:PT}}"
rent: "{{VALUE:RENT}}"
council-tax: ""
date: {{DATE}}
---
# `= this.title`

`= ("[Website](" + this.url + ")")`

`=this.img`

| Item             | Info                     |
| ---------------- | ------------------------ |
| Rent             | `=this.rent`             |
| Council tax      | `=this.council-tax`      |
| Address          | `=this.address`          |
| Public transport | `=this.public-transport` |
| Size (sqm)       | `=this.size`             |

Q & A

- [ ] Council tax band: `=this.council-tax`
- [ ] Damp/moldy?
- [ ] Entrance area
- [ ] Garden/outside space
- [ ] Cupboard spaces
- [ ] Kitchen
    - [ ] dish washer
    - [ ] fridge
    - [ ] work surfaces
- [ ] Bedrooms
- [ ] Blinds + curtains
- [ ] Floor

Info

Log

