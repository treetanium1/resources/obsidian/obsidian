---
title: "{{VALUE}}"
---
# {{VALUE}}

## References

## Sights

## Visits

```dataview
TABLE WITHOUT ID
file.link AS "Event",
date AS "Date",
(join(tags)) AS "Tags"
WHERE contains(join(location), this.title)
```
