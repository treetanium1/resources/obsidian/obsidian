---
title:
fqdn: 
ip: 
tags:
  - 
category:
  - machine/
type: 
description: |
  ...
boot:
maintainer:
  - {{author}}
---

# `=this.title`

```dataview
TABLE WITHOUT ID
ip AS "IP",
fqdn AS "FQDN",
type AS "Type",
boot AS "Boot"
WHERE file.path = this.file.path
```
